#ifndef MQTT_H
#define MQTT_H

#include "mqttpacket.h"
#include <QObject>
#include <QtWebSockets/QWebSocket>

class MqttClient : public QObject {
	Q_OBJECT
public:
	explicit MqttClient(QObject *parent = nullptr);
	~MqttClient();
	void start(const QUrl &url);
	void stop();
	void sendData(const QByteArray &data);

	void connect(const QString &name);
	void publishMsg(const QString &topic, const QString &msg);
	void subscribe(const QString &topic);
	void unsubscribe(const QString &topic);
	void createTopic(const QString &topic);
	void fetchTopics();
	void fetchClients(const QString &topic);

signals:
	void connected();
	void disconnected();
	void error(QString error);

	void connack(bool result, const QString &info);
	void publish(const QString &topic, const QString &msg);
	void suback(bool result, const QString &info);
	void unsuback(bool result, const QString &info);
	void topicack(bool result, const QString &info);
	void topiclistack(const QStringList &topicList);
	void topicuserack(const QStringList &clientList);
	void notifytopic(const QString &topic);
	void notifysub(const QString &topic, const QString &name);
	void notifyunsub(const QString &topic, const QString &name);

private Q_SLOTS:
	void onConnect();
	void onDisconnect();
	void onError(QAbstractSocket::SocketError error);
	void onMessageReceived(const QByteArray &msg);
	void onSslErrors(const QList<QSslError> &errors);

private:
	QWebSocket *socket;
};

#endif // MQTT_H
