#include "mqtt.h"

MqttClient::MqttClient(QObject *parent) : QObject(parent) {
	socket = new QWebSocket();
	QObject::connect(socket, &QWebSocket::connected, this, &MqttClient::onConnect);
	QObject::connect(socket, &QWebSocket::disconnected, this, &MqttClient::onDisconnect);
	QObject::connect(socket, &QWebSocket::binaryMessageReceived, this, &MqttClient::onMessageReceived);
	QObject::connect(socket, QOverload<QAbstractSocket::SocketError>::of(&QWebSocket::error), this, &MqttClient::onError);
	QObject::connect(socket, QOverload<const QList<QSslError> &>::of(&QWebSocket::sslErrors), this, &MqttClient::onSslErrors);
}

MqttClient::~MqttClient() {
	socket->deleteLater();
}

void MqttClient::start(const QUrl &url) {
	qDebug() << "Connecting to " << url;
	socket->open(url);
}

void MqttClient::stop() {
	qDebug() << "Closing socket->..";
	socket->close();
}

void MqttClient::sendData(const QByteArray &data) {
	socket->sendBinaryMessage(data);
}

void MqttClient::connect(const QString &name) {
	MqttPacket packet;
	packet.connect(name);
	sendData(packet.raw());
}

void MqttClient::publishMsg(const QString &topic, const QString &msg) {
	MqttPacket packet;
	packet.publish(topic, msg);
	sendData(packet.raw());
}

void MqttClient::subscribe(const QString &topic) {
	MqttPacket packet;
	packet.subscribe(topic);
	sendData(packet.raw());
}

void MqttClient::unsubscribe(const QString &topic) {
	MqttPacket packet;
	packet.unsubscribe(topic);
	sendData(packet.raw());
}

void MqttClient::createTopic(const QString &topic) {
	MqttPacket packet;
	packet.topic(topic);
	sendData(packet.raw());
}

void MqttClient::fetchTopics() {
	MqttPacket packet;
	packet.topiclist();
	sendData(packet.raw());
}

void MqttClient::fetchClients(const QString &topic) {
	MqttPacket packet;
	packet.topicuser(topic);
	sendData(packet.raw());
}

void MqttClient::onConnect() {
	emit connected();
}

void MqttClient::onDisconnect() {
	emit disconnected();
}

void MqttClient::onMessageReceived(const QByteArray &msg) {
	MqttPacket packet(msg);

	switch (packet) {
	case MqttPacket::Connack:
		emit connack(packet["result"].toBool(), packet["info"].toString());
		break;
	case MqttPacket::Publish:
		emit publish(packet["topic"].toString(), packet["msg"].toString());
		break;
	case MqttPacket::Suback:
		emit suback(packet["result"].toBool(), packet["info"].toString());
		break;
	case MqttPacket::Unsuback:
		emit unsuback(packet["result"].toBool(), packet["info"].toString());
		break;
	case MqttPacket::Topicack:
		emit topicack(packet["result"].toBool(), packet["info"].toString());
		break;
	case MqttPacket::Topiclistack:
		emit topiclistack(packet["body"].toStringList());
		break;
	case MqttPacket::Topicuserack:
		emit topicuserack(packet["body"].toStringList());
		break;
	case MqttPacket::Notifytopic:
		emit notifytopic(packet["body"].toString());
		break;
	case MqttPacket::Notifysub:
		emit notifysub(packet["topic"].toString(), packet["name"].toString());
		break;
	case MqttPacket::Notifyunsub:
		emit notifyunsub(packet["topic"].toString(), packet["name"].toString());
		break;
	case MqttPacket::Connect:
	case MqttPacket::Subscribe:
	case MqttPacket::Unsubscribe:
	case MqttPacket::Topic:
	case MqttPacket::Topiclist:
	case MqttPacket::Topicuser:
	default:
		break;
	}
}

void MqttClient::onError(QAbstractSocket::SocketError code) {
	Q_UNUSED(code);
	emit error(socket->errorString());
}

void MqttClient::onSslErrors(const QList<QSslError> &errors) {
	Q_UNUSED(errors);
	socket->ignoreSslErrors();
}
