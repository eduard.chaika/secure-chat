#include "mqttpacket.h"
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

MqttPacket::MqttPacket(const QByteArray &rawPacket) : packetType(Null) {
	QJsonDocument jsonDocument(QJsonDocument::fromJson(rawPacket));

	if (jsonDocument.isNull())
		return;

	if (!jsonDocument.isObject())
		return;

	packet = jsonDocument.object();
	if (!packet.contains("type") || !packet.contains("body"))
		return;

	QJsonObject &&body = packet["body"].toObject();

	switch (packet["type"].toInt()) {
	case Connack:
	case Topicack:
	case Suback:
	case Unsuback:
		if (!body.contains("result") || !body.contains("info"))
			return;
		break;
	case Publish:
		if (!body.contains("topic") || !body.contains("msg"))
			return;
		break;
	case Connect:
	case Subscribe:
	case Unsubscribe:
	case Topic:
	case Topiclist:
	case Topicuser:
	case Notifytopic:
	case Notifysub:
	case Notifyunsub:
		break;
	case Topiclistack:
	case Topicuserack:
		if (!packet["body"].isArray())
			return;
		break;
	default:
		return;
		break;
	}

	packetType = packet["type"].toInt();
}

const QByteArray &MqttPacket::raw() {
	if (rawPacket.length())
		return rawPacket;

	return (rawPacket = QJsonDocument(packet).toJson());
}

void MqttPacket::setPacket(Type type, const QString &body) {
	packetType = type;
	packet.insert("type", packetType);
	packet.insert("body", body);
}

void MqttPacket::setAckPacket(Type type, bool result, const QString &info) {
	packetType = type;
	packet.insert("type", packetType);
	packet.insert("body", QJsonObject{
							  {"info", info},
							  {"result", result},
						  });
}

void MqttPacket::setListPacket(Type type, const QStringList &list) {
	QJsonArray array;
	for (auto &item : list)
		array.append(item);

	packetType = type;
	packet.insert("type", packetType);
	packet.insert("body", array);
}

void MqttPacket::setNotifyPacket(Type type, const QString &topic, const QString &name) {
	packetType = type;
	packet.insert("type", packetType);
	packet.insert("body", QJsonObject{
							  {"name", name},
							  {"topic", topic},
						  });
}

void MqttPacket::publish(const QString &topic, const QString &msg) {
	packetType = Publish;
	packet.insert("type", packetType);
	packet.insert("body", QJsonObject{
							  {"msg", msg},
							  {"topic", topic},
						  });
}
