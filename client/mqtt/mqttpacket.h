#ifndef MQTTPACKET_H
#define MQTTPACKET_H

#include <QJsonObject>
#include <QVariant>

struct MqttPacket {
	enum Type : int {
		Null,
		/**
		 * @brief
		 * When a client wants to log in,
		 * he sends his name in Connect packet.
		 */
		Connect,

		/**
		 * @brief
		 * Server response to Connect request.
		 * If successful, true is returned, otherwise false.
		 * When authorization is unsuccessful, the 'info' field contains the reason.
		 */
		Connack,

		/**
		 * @brief
		 * Request to publish a message in a specific topic.
		 */
		Publish,

		/**
		 * @brief
		 * Client request to subscribe to a specific topic.
		 */
		Subscribe,

		/**
		 * @brief
		 * Response to client Subscribe request.
		 * If successful, true is returned, otherwise false.
		 * When request fails, the 'info' field contains the reason.
		 */
		Suback,

		/**
		 * @brief
		 * Client request to unsubscribe from topic.
		 */
		Unsubscribe,

		/**
		 * @brief
		 * Response to client unsubscribe request.
		 * If successful, true is returned, otherwise false.
		 */
		Unsuback,

		/**
		 * @brief
		 * Client request to create a new topic.
		 */
		Topic,

		/**
		 * @brief
		 * Response to client to create a topic request.
		 * If successful, true is returned, otherwise false.
		 * When request fails, the 'info' field contains the reason.
		 */
		Topicack,

		/**
		 * @brief
		 * Client request to get a list of existing topics.
		 */
		Topiclist,

		/**
		 * @brief
		 * Response to client to topic list request.
		 */
		Topiclistack,

		/**
		 * @brief
		 * Client request to get a list of users in specific topic.
		 */
		Topicuser,

		/**
		 * @brief
		 * Response to client to user list request.
		 */
		Topicuserack,

		/**
		 * @brief
		 * When user creates a new topic,
		 * all users receive a Notifytopic.
		 */
		Notifytopic,

		/**
		 * @brief
		 * When user joins the topic,
		 * all users in topic receive a Notifysub.
		 */
		Notifysub,

		/**
		 * @brief
		 * When user disconnects from the topic,
		 * all users in topic receive a Notifyunsub.
		 */
		Notifyunsub,
	};

	MqttPacket() : packetType(Null) {}
	MqttPacket(const QByteArray &packet);
	const QByteArray &raw();

	void connect(const QString &name) { setPacket(Connect, name); }
	void connack(bool result, const QString &info) { setAckPacket(Connack, result, info); }
	void publish(const QString &topic, const QString &msg);
	void subscribe(const QString &topic) { setPacket(Subscribe, topic); }
	void suback(bool result, const QString &info) { setAckPacket(Suback, result, info); }
	void unsubscribe(const QString &topic) { setPacket(Unsubscribe, topic); }
	void unsuback(bool result, const QString &info) { setAckPacket(Unsuback, result, info); }
	void topic(const QString &topic) { setPacket(Topic, topic); }
	void topicack(bool result, const QString &info) { setAckPacket(Topicack, result, info); }
	void topiclist() { setPacket(Topiclist, ""); }
	void topiclistack(const QStringList &topicList) { setListPacket(Topiclistack, topicList); }
	void topicuser(const QString &topic) { setPacket(Topicuser, topic); }
	void topicuserack(const QStringList &clientList) { setListPacket(Topicuserack, clientList); }
	void notifytopic(const QString &topic) { setPacket(Notifytopic, topic); }
	void notifysub(const QString &topic, const QString &name) { setNotifyPacket(Notifysub, topic, name); }
	void notifyunsub(const QString &topic, const QString &name) { setNotifyPacket(Notifyunsub, topic, name); }

	operator int() { return packetType; }
	bool operator==(int type) { return packetType == type; }
	bool operator!=(int type) { return !(*this == type); }
	QVariant operator[](const char *key) {
		switch (packetType) {
		case Connack:
		case Publish:
		case Suback:
		case Unsuback:
		case Topicack:
		case Notifysub:
		case Notifyunsub:
			return packet["body"].toObject()[key].toVariant();
			break;
		default:
			return packet[key].toVariant();
			break;
		}
	}

private:
	void setPacket(Type type, const QString &body);
	void setAckPacket(Type type, bool result, const QString &info);
	void setListPacket(Type type, const QStringList &list);
	void setNotifyPacket(Type type, const QString &topic, const QString &name);

private:
	int packetType;
	QJsonObject packet;
	QByteArray rawPacket;
};

#endif // MQTTPACKET_H
