#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
	ui->setupUi(this);

	// Define default room
	currentRoom = "default";

	roomList = new StringList();
	clientList = new StringList();

	ui->roomList->setModel(roomList);
	ui->clientList->setModel(clientList);

	ui->roomList->setEditTriggers(QAbstractItemView::NoEditTriggers);
	ui->clientList->setEditTriggers(QAbstractItemView::NoEditTriggers);
	ui->msgList->setEditTriggers(QAbstractItemView::NoEditTriggers);

	// Bind MqttClient to Main window
	connect(&mqttClient, &MqttClient::connected, this, &MainWindow::onConnect);
	connect(&mqttClient, &MqttClient::disconnected, this, &MainWindow::onDisconnect);
	connect(&mqttClient, &MqttClient::error, this, &MainWindow::onError);
	connect(&mqttClient, &MqttClient::connack, this, &MainWindow::onConnack);
	connect(&mqttClient, &MqttClient::publish, this, &MainWindow::onPublish);
	connect(&mqttClient, &MqttClient::suback, this, &MainWindow::onSuback);
	connect(&mqttClient, &MqttClient::topicack, this, &MainWindow::onTopicack);
	connect(&mqttClient, &MqttClient::topiclistack, this, &MainWindow::onTopiclistack);
	connect(&mqttClient, &MqttClient::topicuserack, this, &MainWindow::onTopicuserack);
	connect(&mqttClient, &MqttClient::notifytopic, this, &MainWindow::onNotifytopic);
	connect(&mqttClient, &MqttClient::notifysub, this, &MainWindow::onNotifysub);
	connect(&mqttClient, &MqttClient::notifyunsub, this, &MainWindow::onNotifyunsub);
}

MainWindow::~MainWindow() {
	delete ui;
}

inline bool MainWindow::roomExists(const QString &name) {
	return chats.contains(name);
}

inline void MainWindow::createRoom(const QString &name) {
	chats[name] = new Chat();
}

inline void MainWindow::switchRoom(const QString &name) {
	ui->msgList->setModel(chats[name]->getModel());
}

void MainWindow::onConnect() {
	// Send user name to server
	mqttClient.connect(ui->userEdit->text());
}

void MainWindow::onDisconnect() {
	// Clear all fields
	roomList->setStringList(QStringList());
	clientList->setStringList(QStringList());

	if (!roomExists(currentRoom))
		createRoom(currentRoom);

	switchRoom(currentRoom);
	chats[currentRoom]->publish(Chat::Info, "Disconnected from server!");
}

void MainWindow::onError(QString error) {
	if (!roomExists(currentRoom))
		createRoom(currentRoom);

	switchRoom(currentRoom);
	chats[currentRoom]->publish(Chat::Error, error);
}

void MainWindow::onConnack(bool result, const QString &info) {
	if (!result) {
		emit onError("Connection failed: " + info);
		return;
	}

	// Get available rooms from server
	mqttClient.fetchTopics();
}

void MainWindow::onPublish(const QString &topic, const QString &msg) {
	Q_UNUSED(topic)

	qDebug() << topic << msg;
	chats[topic]->publish(Chat::Message, msg);
}

void MainWindow::onSuback(bool result, const QString &info) {
	if (!result)
		return;

	// Save room name, get list of clients in room
	currentRoom = info;
	clientList->setStringList(QStringList());
	mqttClient.fetchClients(currentRoom);

	// Print info about joining the room once
	if (!roomExists(currentRoom)) {
		createRoom(currentRoom);
		chats[currentRoom]->publish(Chat::Info, "Entering the room <" + currentRoom + ">");
	}

	// Enter new room
	switchRoom(currentRoom);
	ui->chatLabel->setText("Chat room <" + currentRoom + ">");
}

void MainWindow::onTopicack(bool result, const QString &info) {
	if (result)
		return;

	emit onError("Unable to create new room: " + info);
}

void MainWindow::onTopiclistack(const QStringList &topicList) {
	// Fetching a list of available rooms
	roomList->setStringList(topicList);
}

void MainWindow::onTopicuserack(const QStringList &userList) {
	// Fetching a list of clients in room
	clientList->setStringList(userList);
}

void MainWindow::onNotifytopic(const QString &topic) {
	// New room created, add it to the list
	roomList->append(topic);
}

void MainWindow::onNotifysub(const QString &topic, const QString &name) {
	// New user connected to the topic, add user to list
	chats[currentRoom]->publish(Chat::Info, "User <" + name + "> joined the room <" + topic + ">");
	clientList->append(name);
}

void MainWindow::onNotifyunsub(const QString &topic, const QString &name) {
	// User has disconnected from the topic, remove user from list
	chats[currentRoom]->publish(Chat::Info, "User <" + name + "> has disconnected from <" + topic + ">");
	clientList->removeAll(name);
}

void MainWindow::on_connectButton_clicked() {
	if (ui->userEdit->text().length() < 4) {
		emit onError("Too short user name!");
		return;
	}

	if (ui->addrEdit->text().length() < 4) {
		emit onError("Invalid server address!");
		return;
	}

	mqttClient.start(QUrl(ui->addrEdit->text()));
}

void MainWindow::on_roomList_doubleClicked(const QModelIndex &index) {
	QString selectedRoom = roomList->data(index).toString();
	if (selectedRoom != currentRoom) {
		mqttClient.unsubscribe(currentRoom);
		mqttClient.subscribe(selectedRoom);
	}
}

void MainWindow::on_roomButton_clicked() {
	mqttClient.createTopic(ui->roomEdit->text());
}

void MainWindow::on_sendButton_clicked() {
	if (currentRoom.length() && ui->msgEdit->text().length())
		mqttClient.publishMsg(currentRoom, ui->msgEdit->text());
}
