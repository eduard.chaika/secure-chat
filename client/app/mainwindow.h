#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QAbstractButton>
#include <QMainWindow>
#include <QMessageBox>
#include <QStringListModel>
#include <mqtt/mqtt.h>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class StringList : public QStringListModel {
public:
	void append(const QString &string) {
		insertRows(rowCount(), 1);
		setData(index(rowCount() - 1), string);
	}

	void removeAll(const QString &string) {
		QStringList list = stringList();
		if (!list.contains(string))
			return;

		list.removeAll(string);
		setStringList(list);
	}

	StringList &operator<<(const QString &string) {
		append(string);
		return *this;
	}
};

class Chat {
public:
	enum : int {
		Error,
		Info,
		Message
	};

public:
	explicit Chat() { chat = new StringList(); }
	~Chat() { delete chat; }

	void publish(int type, const QString &msg) {
		switch (type) {
		case Error:
			chat->append(QString() + "*** Error *** " + msg);
			break;
		case Info:
			chat->append(QString() + "*** Info *** " + msg);
			break;
		case Message:
			chat->append(msg);
			break;
		default:
			break;
		}
	}

	QStringListModel *getModel() { return chat; }

private:
	StringList *chat;
};

class MainWindow : public QMainWindow {
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private slots:
	void on_connectButton_clicked();
	void on_roomList_doubleClicked(const QModelIndex &index);
	void on_roomButton_clicked();
	void on_sendButton_clicked();

public slots:
	void onConnect();
	void onDisconnect();
	void onError(QString error);

	void onConnack(bool result, const QString &info);
	void onPublish(const QString &topic, const QString &msg);
	void onSuback(bool result, const QString &info);
	void onTopicack(bool result, const QString &info);
	void onTopiclistack(const QStringList &topicList);
	void onTopicuserack(const QStringList &clientList);
	void onNotifytopic(const QString &topic);
	void onNotifysub(const QString &topic, const QString &name);
	void onNotifyunsub(const QString &topic, const QString &name);

private:
	bool roomExists(const QString &name);
	void createRoom(const QString &name);
	void switchRoom(const QString &name);

private:
	Ui::MainWindow *ui;
	MqttClient mqttClient;
	QString currentRoom;
	StringList *roomList, *clientList;
	QMap<QString, Chat *> chats;
};
#endif // MAINWINDOW_H
