#include "securechat.h"
#include <QCoreApplication>
#include <QDebug>

void usage() {
	qDebug() << "Usage:";
	qDebug() << "securechat-server <port>";
	qDebug() << "port - server port [1024..65535]";
}

bool getPort(const QStringList &arguments, quint16 &port) {
	bool result = false;

	if (arguments.size() != 2) {
		qDebug() << "Invalid arguments!";
		return false;
	}

	port = arguments.at(1).toInt(&result);
	if (!result) {
		qDebug() << "Invalid port!";
		return false;
	}

	return true;
}

int main(int argc, char *argv[]) {
	quint16 port;
	QCoreApplication application(argc, argv);

	if (!getPort(application.arguments(), port)) {
		usage();
		return 1;
	}

	SecureChat secureChat(port);
	return application.exec();
}
