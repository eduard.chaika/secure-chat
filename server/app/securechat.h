#ifndef SECURECHAT_H
#define SECURECHAT_H

#include <QObject>
#include <mqtt/mqtt.h>

class SecureChat : public QObject {
	Q_OBJECT

public:
	SecureChat(quint16 port, QObject *parent = nullptr);
	~SecureChat();

private:
	MqttBroker *broker;
};
#endif // SECURECHAT_H
