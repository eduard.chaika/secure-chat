#include "securechat.h"
#include "mqtt/mqtt.h"

SecureChat::SecureChat(quint16 port, QObject *parent) : QObject(parent) {
	broker = new MqttBroker(port);
}

SecureChat::~SecureChat() {
	delete broker;
}
