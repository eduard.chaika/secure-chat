#ifndef MQTT_H
#define MQTT_H

#include "mqttpacket.h"
#include <QMap>
#include <QObject>

class QWebSocketServer;
class QWebSocket;
class QSslError;

class MqttClient : public QObject {
	Q_OBJECT
public:
	explicit MqttClient(QWebSocket *socket, QObject *parent = nullptr);
	~MqttClient();
	void sendData(const QByteArray &data);
	void authorize(const QString &name);
	bool isAuthorized();
	const QString &getName();

signals:
	void disconnected(MqttClient *client);
	void packetReceived(MqttClient *client, MqttPacket &packet);

private slots:
	void onDisconnect();
	void onMessage(const QByteArray &msg);

private:
	bool authorized;
	QString name;
	QStringList subscriptions;
	QWebSocket *socket;
};

class MqttListener : public QObject {
	Q_OBJECT

public:
	explicit MqttListener(quint16 port, QObject *parent = nullptr);
	~MqttListener();
	bool start();

signals:
	void newMqttClient(MqttClient *client);

private slots:
	void onClientConnect();

private:
	quint16 port;
	QWebSocketServer *server;
};

class MqttBroker : public QObject {
	Q_OBJECT
public:
	explicit MqttBroker(quint16 port, QObject *parent = nullptr);

private slots:
	void onClientConnect(MqttClient *client);
	void onClientDisconnect(MqttClient *client);
	void onClientPacket(MqttClient *client, MqttPacket &packet);

private:
	bool isNameAvailable(const QString &name);
	bool createTopic(const QString &topic);
	bool subscribe(MqttClient *client, const QString &topic);
	bool unsubscribe(MqttClient *client, const QString &topic);
	void unsubscribeFromAll(MqttClient *client);
	void publish(MqttPacket &packet);
	void publish(const QString &topic, MqttPacket &packet);
	void publish(const QString &excludeClient, const QString &topic, MqttPacket &packet);
	QStringList getTopicList();
	QStringList getClientList(const QString &topic);

private:
	MqttListener *listener;
	QList<MqttClient *> clients;
	QMap<QString, QList<MqttClient *> > topics;
};

#endif // MQTT_H
