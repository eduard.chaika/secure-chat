#include "mqtt.h"
#include "mqtterror.h"
#include <QFile>
#include <QWebSocket>
#include <QWebSocketServer>
#include <QtNetwork/QSslCertificate>
#include <QtNetwork/QSslKey>

MqttClient::MqttClient(QWebSocket *socket, QObject *parent) : QObject(parent), authorized(false) {
	this->socket = socket;
	connect(socket, &QWebSocket::disconnected, this, &MqttClient::onDisconnect);
	connect(socket, &QWebSocket::binaryMessageReceived, this, &MqttClient::onMessage);
}

MqttClient::~MqttClient() {
	socket->deleteLater();
}

void MqttClient::sendData(const QByteArray &data) {
	socket->sendBinaryMessage(data);
}

bool MqttClient::isAuthorized() {
	return authorized;
}

void MqttClient::authorize(const QString &name) {
	this->name = name;
	authorized = true;
}

const QString &MqttClient::getName() {
	return name;
}

void MqttClient::onDisconnect() {
	emit disconnected(this);
}

void MqttClient::onMessage(const QByteArray &msg) {
	MqttPacket packet(msg);
	if (packet != MqttPacket::Null)
		emit packetReceived(this, packet);
}

MqttListener::MqttListener(quint16 port, QObject *parent) : QObject(parent), port(port) {
	server = new QWebSocketServer(QStringLiteral("SecureChat Server"), QWebSocketServer::SecureMode, this);

	QSslConfiguration sslConfig;

	QFile certFile(QStringLiteral(":/ssl/ssl.cert"));
	QFile keyFile(QStringLiteral(":/ssl/ssl.key"));
	certFile.open(QIODevice::ReadOnly);
	keyFile.open(QIODevice::ReadOnly);

	QSslCertificate certificate(&certFile, QSsl::Pem);
	QSslKey sslKey(&keyFile, QSsl::Rsa, QSsl::Pem);
	sslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);
	sslConfig.setLocalCertificate(certificate);
	sslConfig.setPrivateKey(sslKey);

	server->setSslConfiguration(sslConfig);
}

MqttListener::~MqttListener() {
	server->deleteLater();
}

bool MqttListener::start() {
	if (!server->listen(QHostAddress::Any, port)) {
		// Is it possible?
		qDebug() << "Listener launch failed!";
		return false;
	}

	qDebug() << "Server listening on port" << port;
	connect(server, &QWebSocketServer::newConnection, this, &MqttListener::onClientConnect);
	connect(server, &QWebSocketServer::sslErrors, this, [](const QList<QSslError> &errors) {
		Q_UNUSED(errors);
		qDebug() << "SSL Errors";
	});

	return true;
}

void MqttListener::onClientConnect() {
	QWebSocket *socket = server->nextPendingConnection();
	qDebug() << "New client connected";
	emit newMqttClient(new MqttClient(socket));
}

MqttBroker::MqttBroker(quint16 port, QObject *parent) : QObject(parent) {
	// Create default channel for new users
	topics["default"];

	listener = new MqttListener(port, this);
	if (listener->start()) {
		connect(listener, &MqttListener::newMqttClient, this, &MqttBroker::onClientConnect);
	}
}

void MqttBroker::onClientConnect(MqttClient *client) {
	clients << client;
	connect(client, &MqttClient::packetReceived, this, &MqttBroker::onClientPacket);
	connect(client, &MqttClient::disconnected, this, &MqttBroker::onClientDisconnect);
}

void MqttBroker::onClientDisconnect(MqttClient *client) {
	MqttPacket packet;

	qDebug() << "User disconnected: " << client->getName();

	// Notify all users in this topic that the user has disconnected
	for (auto &topic : topics.keys()) {
		if (topics[topic].contains(client)) {
			packet.notifyunsub(topic, client->getName());
			publish(topic, packet);
		}
	}

	unsubscribeFromAll(client);
	clients.removeAll(client);
	delete client;
}

void MqttBroker::onClientPacket(MqttClient *client, MqttPacket &packet) {
	MqttPacket ack;

	switch (packet) {
	case MqttPacket::Connect: {
		QString clientName = packet["body"].toString();
		if (!isNameAvailable(clientName)) {
			ack.connack(false, MqttError::getError(MqttError::NameExists));
			client->sendData(ack.raw());
		} else {
			// Authorize user
			qDebug() << "User" << clientName << "has authorized";
			ack.connack(true, clientName);
			client->authorize(clientName);
			client->sendData(ack.raw());

			// Subscribe user to channel 'default'
			MqttPacket suback;
			subscribe(client, "default");
			suback.suback(true, "default");
			client->sendData(suback.raw());

			// Notify all users in default room about newbie
			MqttPacket notify;
			notify.notifysub("default", clientName);
			publish(client->getName(), "default", notify);
		}
	} break;
	case MqttPacket::Connack:
	case MqttPacket::Suback:
	case MqttPacket::Topicack:
	case MqttPacket::Topiclistack:
	case MqttPacket::Notifysub:
	case MqttPacket::Notifyunsub:
		break;
	case MqttPacket::Publish:
		ack.publish(packet["topic"].toString(), "<" + client->getName() + ">: " + packet["msg"].toString());
		qDebug() << client->getName() << "publish" << packet["msg"].toString() << "into" << ack["topic"].toString();
		publish(ack["topic"].toString(), ack);
		break;
	case MqttPacket::Subscribe:
		if (subscribe(client, packet["body"].toString())) {
			qDebug() << "User" << client->getName() << " has subscribed to " << packet["body"].toString();
			ack.suback(true, packet["body"].toString());
			client->sendData(ack.raw());

			MqttPacket notify;
			notify.notifysub(packet["body"].toString(), client->getName());
			publish(client->getName(), packet["body"].toString(), notify);
		} else {
			qDebug() << "User" << client->getName() << "subscription to" << packet["body"].toString() << "failed!";
			ack.suback(false, MqttError::getError(MqttError::UnknownTopic));
			client->sendData(ack.raw());
		}
		break;
	case MqttPacket::Unsubscribe:
		if (unsubscribe(client, packet["body"].toString())) {
			qDebug() << "User" << client->getName() << " has unsubscribed from " << packet["body"].toString();
			ack.notifyunsub(packet["body"].toString(), client->getName());
			publish(packet["body"].toString(), ack);
		} else {
			qDebug() << "User" << client->getName() << "unsubscription from" << packet["body"].toString() << "failed!";
		}
		break;
	case MqttPacket::Topic:
		if (createTopic(packet["body"].toString())) {
			// Notify all users on server about new topic
			ack.notifytopic(packet["body"].toString());
			publish(ack);
		} else {
			ack.topicack(false, MqttError::getError(MqttError::TopicExists));
			client->sendData(ack.raw());
		}
		break;
	case MqttPacket::Topiclist:
		ack.topiclistack(getTopicList());
		client->sendData(ack.raw());
		break;
	case MqttPacket::Topicuser:
		ack.topicuserack(getClientList(packet["body"].toString()));
		client->sendData(ack.raw());
		break;
	default:
		break;
	}
}

bool MqttBroker::isNameAvailable(const QString &name) {
	for (auto &topic : topics) {
		for (auto &client : topic) {
			if (client->isAuthorized() && client->getName() == name)
				return false;
		}
	}

	return true;
}

bool MqttBroker::createTopic(const QString &topic) {
	if (topics.contains(topic))
		return false;

	topics[topic];
	return true;
}

void MqttBroker::publish(MqttPacket &packet) {
	for (auto &topic : topics.keys()) {
		publish(topic, packet);
	}
}

void MqttBroker::publish(const QString &topic, MqttPacket &packet) {
	for (auto &client : topics[topic]) {
		client->sendData(packet.raw());
	}
}

void MqttBroker::publish(const QString &excludeClient, const QString &topic, MqttPacket &packet) {
	for (auto &client : topics[topic]) {
		if (client->getName() != excludeClient) {
			client->sendData(packet.raw());
		}
	}
}

bool MqttBroker::subscribe(MqttClient *client, const QString &topic) {
	if (!topics.contains(topic))
		return false;

	if (topics[topic].contains(client))
		return false;

	topics[topic] << client;
	return true;
}

bool MqttBroker::unsubscribe(MqttClient *client, const QString &topic) {
	if (!topics.contains(topic))
		return false;

	topics[topic].removeAll(client);
	return true;
}

void MqttBroker::unsubscribeFromAll(MqttClient *client) {
	for (auto &topic : topics.keys()) {
		unsubscribe(client, topic);
	}
}

QStringList MqttBroker::getTopicList() {
	QStringList topicList;
	for (auto &topic : topics.keys()) {
		topicList.append(topic);
	}

	return topicList;
}

QStringList MqttBroker::getClientList(const QString &topic) {
	QStringList clientList;

	if (topics.contains(topic)) {
		for (auto &client : topics[topic]) {
			clientList.append(client->getName());
		}
	}

	return clientList;
}
