#ifndef MQTTERROR_H
#define MQTTERROR_H

#include <QString>

class MqttError {

public:
	enum : int {
		NameExists,
		TopicExists,
		UnknownTopic,
	};

	static inline const QString getError(int code) {
		switch (code) {
		case NameExists:
			return QString("Name is already in use");
			break;
		case TopicExists:
			return QString("Topic already exists");
			break;
		case UnknownTopic:
			return QString("Invalid topic");
			break;
		}

		return QString("Unknown error");
	}
};

#endif // MQTTERROR_H
