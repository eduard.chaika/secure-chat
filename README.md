# Secure Chat

Client/Server Chat application with SSL security.
- Users can create new chat rooms.
- In one chat room more than two users can communicate.
- Showing notifications when users joined or left chat room.
- Used Qt Framework for client application GUI.
- Compile for Linux and Windows

# Supported Platforms

- Windows
- Linux

# Requirements

- Compiler with C++11 support
- Qt 5

# Building

You can build Secure Chat application through Qt Creator by simply opening the project file and clicking the build button. 

In this example, we will look at the way to build application through the command line.

## Linux

### Preparing
	
	sudo apt-get update
	sudo apt install g++ git build-essential
	sudo apt install qtcreator qt5-default libqt5websockets5-dev

### Building
	
1. Clone repository to your local machine:

        git clone https://gitlab.com/eduard.chaika/secure-chat.git
		
2. Configure & Build application:
        
	Server

		cd secure-chat/server
		qmake sercurechat-server
		make all
        
	Client

		cd secure-chat/client
		qmake sercurechat-client
		make all

## Windows

### Preparing

You may need to add qt tools path to the Windows path variable.

1. On the Windows desktop, right-click My Computer.
2. In the pop-up menu, click Properties.
3. In the System Properties window, click the Advanced tab, and then click Environment Variables.
4. In the System Variables window, highlight Path, and click Edit.	5. On Windows 10 click new; On Window 7 insert the cursor at the end of the Variable value field.
7. Type the full path to the qt tool folder:
	
	For example: C:\Qt\Tools\mingw810_64\bin
8. On Windows 7 add semicolor after added path.
9. Click OK in each open window.

### Building

1. Clone repository to your local machine:

        git clone https://gitlab.com/eduard.chaika/secure-chat.git
	
2. Configure & Build application:
        
	Server

        cd secure-chat/server
		C:\path\to\qt\bin\qmake.exe -o Makefile securechat-server.pro -spec win32-g++ "CONFIG+=..."
		mingw32-make -f Makefile.Debug

	Client

        cd secure-chat/client
		C:\path\to\qt\bin\qmake.exe -o Makefile securechat-client.pro -spec win32-g++ "CONFIG+=..."
		mingw32-make -f Makefile.Debug

	Deploy

	    C:\path\to\qt\bin\windeployqt.exe path/to/securechat-server.exe

    Example

        cd secure-chat/server
        C:\Qt\5.15.0\mingw81_64\bin\qmake.exe -o Makefile securechat-client.pro -spec win32-g++ "CONFIG+=..."
	    mingw32-make -f Makefile.Debug
        C:\Qt\5.15.0\mingw81_64\bin\windeployqt.exe .\securechat-client.exe

    > Note: You can build Release version by replacing Makefile.Debug with  Makefile.Release


# Usage

## Server

### Linux
In bash terminal type:

    admin@desktop:~$ <path-to-file>\securechat-server <port>

### Windows
Open your favorite command-line tool and type:

    C:\Users\Admin> <path-to-file>\securechat-server.exe <port>

_port_ - server port in range from 1024 to 65535
